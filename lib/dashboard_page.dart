// ignore_for_file: prefer_const_constructors, unused_import, avoid_unnecessary_containers

import 'package:flutter/material.dart';
import 'package:module_3/comment_page.dart';
import 'package:module_3/edit_page.dart';
import 'package:module_3/register_page1.dart';
import 'package:flutter/services.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  State<DashboardPage> createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        backgroundColor: Color.fromARGB(255, 255, 255, 255),
        appBar: AppBar(
          title: const Text("Dashboard"),
          actions: <Widget>[
            IconButton(
              icon: const Icon(Icons.comment),
              tooltip: 'Comment Icon',
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const CommentPage()),
                );
              },
            ), //IconButton
            IconButton(
              icon: const Icon(Icons.edit),
              tooltip: 'Edit Icon',
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const EditPage()),
                );
              },
            ), //IconButton
          ], //<Widget>[]
          backgroundColor: Color.fromARGB(255, 30, 41, 36),
          elevation: 50.0,
          leading: IconButton(
            icon: const Icon(Icons.person),
            tooltip: 'Person Icon',
            onPressed: () {},
          ),
          systemOverlayStyle: SystemUiOverlayStyle.light,
        ), //AppBar
        body: const Center(
          child: Text(
            "App underconstruction",
            style: TextStyle(fontSize: 24),
          ), //Text
        ), //Center
      ), //Scaffold
      //Removing Debug Banner
    );
  }
}
