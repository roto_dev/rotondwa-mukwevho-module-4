// ignore_for_file: unused_import

import 'package:flutter/material.dart';
import 'package:module_3/edit_page.dart';
import 'package:module_3/register_page1.dart';
import 'package:flutter/services.dart';

class CommentPage extends StatefulWidget {
  const CommentPage({Key? key}) : super(key: key);

  @override
  State<CommentPage> createState() => _CommentPageState();
}

class _CommentPageState extends State<CommentPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        backgroundColor: Color.fromARGB(255, 254, 253, 253),
        appBar: AppBar(
          title: const Text('Comment page'),
          backgroundColor: Color.fromARGB(255, 52, 66, 60),
        ), //AppBar
      ), //Scaffold
      //Removing Debug Banner
    );
  }
}
