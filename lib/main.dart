// ignore_for_file: prefer_const_constructors, unused_import

import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:module_3/splash_page.dart';
import 'login_page.dart';
import 'register_page1.dart';
import 'comment_page.dart';
import 'confirm_page.dart';
import 'dashboard_page.dart';
import 'edit_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      themeMode: ThemeMode.light,
      home: AnimatedSplashScreen(
          splash: Icons.home,
          duration: 2000,
          backgroundColor: Color.fromARGB(255, 255, 255, 255),
          nextScreen: LoginPage()),
    );
  }
}
